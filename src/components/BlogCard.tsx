import type { MarkdownInstance } from 'astro';
import { format } from 'date-fns';

import type { IFrontmatter } from '@/types/IFrontMatter';

type IBlogCardProps = {
  instance: MarkdownInstance<IFrontmatter>;
};

export default function BlogCard (props: IBlogCardProps){
  return(
  <a class="hover:translate-y-1" href={props.instance.url}>
    <div class="overflow-hidden rounded-md bg-stone-700">
      <div class="aspect-w-3 aspect-h-2">
        <img
          class="h-full w-full object-cover object-center"
          src={props.instance.frontmatter.imgSrc}
          alt={props.instance.frontmatter.imgAlt}
          loading="lazy"
        />
      </div>

      <div class="px-3 pt-4 pb-6 text-center">
        <h2 class="text-xl font-semibold">
          {props.instance.frontmatter.title}
        </h2>

        <div class="mt-1 text-xs text-gray-400">
          {format(new Date(props.instance.frontmatter.pubDate), 'LLL d, yyyy')}
        </div>

        <div class="mt-2 text-sm">
          {props.instance.frontmatter.description}
        </div>
      </div>
    </div>
  </a>
);
};
