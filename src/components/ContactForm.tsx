import {GradientText} from '@/components'

export default function ContactForm(){
  return(
  <div class="flex flex-col items-center justify-between gap-6 sm:flex-row">
    <div class="sm:w-7/12 text-3xl font-bold">
      <GradientText>Contact Me</GradientText>
      <p class="mt-3 text-gray-300 text-base font-normal">Let's talk about your next project, fill this form to start the business.</p>
    </div>

    <div class="w-full sm:w-5/12">
      <div class="flex items-center">
        <form class="flex flex-col w-full" method="POST" action="https://getform.io/f/1c2160cb-a39b-4a96-99b4-1d19096274e6"> 
          <input class="w-full rounded-lg bg-stone-700 appearance-none px-4 py-2 mb-2 focus:outline-none focus-within:ring-2 focus-within:ring-lime-600 hover:ring-2 hover:ring-lime-600" placeholder="Name" type="text" name="user_name" />
          <input class="w-full rounded-lg bg-stone-700 appearance-none px-4 py-2 mb-2 focus:outline-none focus-within:ring-2 focus-within:ring-lime-600 hover:ring-2 hover:ring-lime-600" placeholder="E-mail" type="email" name="user_email" />
          <textarea name="message" class="w-full rounded-lg bg-stone-700 appearance-none px-4 py-2 focus:outline-none focus-within:ring-2 focus-within:ring-lime-600 hover:ring-2 hover:ring-lime-600" placeholder="Your message here..." />
          <button class="w-full bg-lime-600 mt-2 rounded-full p-2 text-gray-50" type="submit" value="Send">Submit</button>
        </form>
      </div>
    </div>
    </div>
  );
};
