type IFooterCopyrightProps = {
  site_name: string;
};

export default function FooterCopyright (props: IFooterCopyrightProps){
  return(
  <div class="border-t border-gray-600 pt-5">
    <div class="text-sm text-gray-200">
      © Copyright {new Date().getFullYear()} by {props.site_name}. Built with ♥
      by{' '}
      <a
        class="text-yellow-500 hover:underline"
        href="https://mslabs.netlify.app"
        target="_blank"
        rel="noopener noreferrer"
      >
        LasX
      </a>
      .
    </div>
  </div>
);
};
