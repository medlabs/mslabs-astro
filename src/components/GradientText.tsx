import type { JSX } from 'solid-js';

type IGradientTextProps = {
  children: JSX.Element;
};

export default function GradientText (props: IGradientTextProps){
  return(
  <span class="bg-gradient-to-br from-lime-500 to-lime-400 bg-clip-text text-transparent">
    {props.children}
  </span>
);
  };
