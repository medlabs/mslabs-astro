import type { JSX } from 'solid-js';

type IHeroAvatarProps = {
  title: JSX.Element;
  description: JSX.Element;
  avatar: JSX.Element;
  socialButtons: JSX.Element;
};

export default function HeroAvatar (props: IHeroAvatarProps) {
  return (
  <div class="flex flex-col items-center md:flex-row md:justify-between md:gap-x-24">
    <div>
      <h1 class="text-3xl font-bold">{props.title}</h1>

      <p class="mt-6 text-xl leading-9">{props.description}</p>

      <div class="mt-3 flex gap-1">{props.socialButtons}</div>
    </div>

    <div class="shrink-0">{props.avatar}</div>
  </div>
);
};
