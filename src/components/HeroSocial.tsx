type IHeroSocialProps = {
  src: string;
  alt: string;
};

export default function HeroSocial(props: IHeroSocialProps){ 
  return(
  <img
    class="h-12 w-12 hover:translate-y-1"
    src={props.src}
    alt={props.alt}
    loading="lazy"
  />
);
};
