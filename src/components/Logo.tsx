import type { JSX } from 'solid-js';

type ILogoProps = {
  icon: JSX.Element;
  name: string;
};

export default function Logo (props: ILogoProps){
  return (
  <div class="flex items-center bg-gradient-to-br from-yellow-500 to-yellow-400 bg-clip-text text-xl font-bold text-transparent">
    {props.icon}

    {props.name}
  </div>
);
};
