import type { JSX } from 'solid-js';

type INavMenuProps = {
  children: JSX.Element;
};

export default function NavMenu (props: INavMenuProps){
  return(
  <nav>
    <ul class="flex gap-x-3 font-medium text-gray-200">{props.children}</ul>
  </nav>
);
};
