type INavMenuItemProps = {
  href: string;
  children: string;
};

export default function NavMenuItem (props: INavMenuItemProps){
  return(
  <li class="hover:text-white">
    <a href={props.href}>{props.children}</a>
  </li>
);
};
