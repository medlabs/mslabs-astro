import type { JSX } from 'solid-js';

type INavbarProps = {
  children: JSX.Element;
};

export default function NavbarTwoColumns(props: INavbarProps) {
  return(
  <div class="flex flex-col gap-y-3 sm:flex-row sm:items-center sm:justify-between">
    {props.children}
  </div>
);
  };
