import type { FrontmatterPage } from '../types/IFrontMatter';

type INewerOlderPaginationProps = {
  page: FrontmatterPage;
};

export default function NewerOlderPagination (props: INewerOlderPaginationProps){
  return(
  <div class="flex justify-center gap-8">
    {props.page.url.prev && <a href={props.page.url.prev}>← Newer Posts</a>}
    {props.page.url.next && <a href={props.page.url.next}>Older Posts →</a>}
  </div>
);
};
