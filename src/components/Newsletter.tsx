import type { JSX } from 'solid-js';

type INewsletterProps = {
  title: JSX.Element;
  description: JSX.Element;
};

export default function Newsletter(props: INewsletterProps){
  return(
  <div class="flex flex-col items-center justify-between gap-6 sm:flex-row">
    <div class="sm:w-7/12">
      <div class="text-3xl font-bold">{props.title}</div>

      <p class="mt-3 text-gray-300">{props.description}</p>
    </div>

    <div class="w-full sm:w-5/12">
      <form class="flex rounded-full bg-stone-700 px-4 py-2 focus-within:ring-2 focus-within:ring-lime-600 hover:ring-2 hover:ring-lime-600">
        <input class="w-full appearance-none bg-stone-700 focus:outline-none" />

        <button
          class="ml-2 shrink-0 rounded-full bg-gradient-to-br from-lime-600 to-lime-500 px-3 py-1 text-sm font-medium hover:from-lime-700 hover:to-lime-600 focus:outline-none focus:ring-2 focus:ring-lime-600/50"
          type="submit"
        >
          Subscribe
        </button>
      </form>
    </div>
  </div>
);
};
