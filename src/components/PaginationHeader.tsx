type IPaginationHeaderProps = {
  title: string;
  description: string;
};

export default function PaginationHeader (props: IPaginationHeaderProps) {
    return(
  <div class="text-center">
    <h1 class="text-3xl font-bold">{props.title}</h1>

    <div class="mt-3 text-gray-200">{props.description}</div>
  </div>
);
}
