import type { JSX } from 'solid-js';

import type { IFrontmatter } from '../types/IFrontMatter';

type IPostContentProps = {
  content: IFrontmatter;
  children: JSX.Element;
};

export default function PostContent(props: IPostContentProps){
  return (
  <div class="mx-auto mt-5 max-w-prose">
    <div class="aspect-w-3 aspect-h-2">
      <img
        class="h-full w-full rounded-lg object-cover object-center"
        src={props.content.imgSrc}
        alt={props.content.imgAlt}
        loading="lazy"
      />
    </div>

    <div class="prose prose-invert mt-8 prose-img:rounded-lg">
      {props.children}
    </div>
  </div>
);
}
