import { format } from 'date-fns';

import type { IFrontmatter } from '../types/IFrontMatter';

type IPostHeaderProps = {
  content: IFrontmatter;
  author: string;
};

export default function PostHeader(props: IPostHeaderProps){
  return (
  <>
    <h1 class="text-center text-3xl font-bold">{props.content.title}</h1>

    <div class="mt-2 text-center text-sm text-gray-400">
      By {props.author} on{' '}
      {format(new Date(props.content.pubDate), 'LLL d, yyyy')}
    </div>
  </>
);
}
