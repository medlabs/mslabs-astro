import type { JSX } from 'solid-js';
type IProjectProps = {
  img: {
    src: string;
    alt: string;
  };
  name: string;
  description: string;
  link: string;
  category: JSX.Element;
};

export default function Project(props: IProjectProps) {
  return(
  <div class="flex flex-col items-center gap-x-8 rounded-md bg-stone-700 p-3 md:flex-row">
    <div class="shrink-0">
      <a href={props.link} target="_blank">
        <img
          class="h-36 w-36 hover:translate-y-1"
          src={props.img.src}
          alt={props.img.alt}
          loading="lazy"
        />
      </a>
    </div>

    <div>
      <div class="flex flex-col items-center gap-y-2 md:flex-row">
        <a class="hover:text-lime-400" href={props.link} target="_blank">
          <div class="text-xl font-semibold">{props.name}</div>
        </a>

        <div class="ml-3 flex gap-2">{props.category}</div>
      </div>

      <p class="mt-3 text-gray-400">{props.description}</p>
    </div>
  </div>
  );
};
