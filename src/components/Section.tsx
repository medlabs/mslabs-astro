import type {JSX} from 'solid-js';
type ISectionProps = {
  title?: string;
  children: JSX.Element;
};

export default function Section(props: ISectionProps) {
  return (
  <div class="mx-auto max-w-screen-lg px-3 py-6">
    {props.title && (
      <div class="mb-6 text-2xl font-bold">{props.title}</div>
    )}

    {props.children}
  </div>
);
};
