---
layout: '@/layouts/BasePost.astro'
title: Markdown Cheatsheet
description: How to write content in Markdown.
pubDate: 2022-12-04T05:30:00Z
imgSrc: '/assets/images/image-post.jpeg'
imgAlt: 'Image post'
---

Writing blog posts using Wordpress or any other alternative using a WYSIWYG editor is easy and fast but some other blogging platforms or Jamstack websites force you to use Markdown, and this method could be really faster and more efficient when you master the Markup language.  
It's similar to Vim vs Vscode, you could code twice faster on vim if you master the shortcuts.  


Here's the basics of Markdown style:

## Headings

# H1 For example
```markdown
# H1 For Example
```

## H2 For example

```markdown
## H2 For Example
```

### H3 For example

```markdown
### H3 For Example
```

#### H4 For example

```markdown
#### H4 For Example
```

##### H5 For example

```markdown
##### H5 For Example
```

###### H6 For example

```markdown
###### H6 For Example
```

## Emphasis

Emphasis, aka italics, with _asterisks_ (`*`asterisks`*`) or _underscores_ (_underscore_).

Strong emphasis, aka bold, with **asterisks** (**) or **underscores** (__).

Strikethrough uses two tildes (~~). ~~Scratch this.~~

## Blockquotes
`> Blockquotes are very handy in email to emulate reply text.`  
`> This line is part of the same quote. Use (>) character to start a blockquote.`

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break. (there is no > at the beginning of this line.)

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can _put_ **Markdown** into a blockquote.

## Horizontal separator

This is a horizontal separator:

---
```Just insert --- (3 hyphens) in the beginning of the line```

---

## List types

### Ordered list
To start a numbered list use numbers followed by a point then a space (1. ).


1. List item 1
2. List item 2
   1. Nested list item A
   2. Nested list item B
3. List item 3

### Unordered list
To start an unordered list just put a hyphen then a space (- ).  


- List item
- List item
  - Nested list item
  - Nested list item
    - Double nested list item
    - Double nested list item
- List item

### Mixed list

1. First ordered list item
2. Another item
   - Unordered sub-list.
3. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
4. And another item.

## Links
`[Inline-style link](https://www.google.com)`  

[Inline-style link](https://www.google.com)

`[Inline-style link with title](https://www.google.com "Google's Homepage")`

[Inline-style link with title](https://www.google.com "Google's Homepage")

[Reference-style link][arbitrary case-insensitive reference text]

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself].

Some text to show that the reference links can follow later.


```
[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com
```
## Images

Images included in _\_posts_ folder are lazy loaded.

Inline-style:
![alt text](/src/images/random.jpeg 'Logo Title Text 1')

## Table

And this is how to create tables in Markdown.  

```markdown
| Tables        | Are           | Cool |
| ------------- |:-------------:| ----:|
| col 3 is      | right-aligned | 1600 |
| col 2 is      | centered      | 12   |
| zebra stripes | are neat      | 1    |

| Markdown | Less      | Pretty     |
| -------- | --------- | ---------- |
| _Still_  | `renders` | **nicely** |
| 1        | 2         | 3          |
```
Result:

| Tables        | Are           | Cool |
| ------------- |:-------------:| ----:|
| col 3 is      | right-aligned | 1600 |
| col 2 is      | centered      | 12   |
| zebra stripes | are neat      | 1    |

| Markdown | Less      | Pretty     |
| -------- | --------- | ---------- |
| _Still_  | `renders` | **nicely** |
| 1        | 2         | 3          |


## Syntax highlight

To add a code block with a specific language highlighting, use "```language_name" put your code then close with ``` 

```javascript
var s = 'JavaScript syntax highlighting'; var s = 'JavaScript syntax highlighting';
alert(s);
```

Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur vero esse non molestias eos excepturi, inventore atque cupiditate. Sed voluptatem quas omnis culpa, et odit.

```python
s = "Python syntax highlighting"
print s
```
