---
layout: '@/layouts/BasePost.astro'
title: Play with time on Vlang
description: Examples of time mudule from vlang standard library
pubDate: 2024-04-01T21:30:00Z
imgSrc: '/assets/images/vlang_time.png'
imgAlt: 'vlang time module'
---
Ah, Vlang. The programming language that's so fast and elegant, it makes even the most verbose languages feel like they're wading through molasses. Despite being objectively superior (wink wink), Vlang inexplicably suffers from a **vocal minority** of haters amongst its more… sluggish… competitors. But hey, who needs friends when you have speed and efficiency on your side, right?
*(ok that was a gemini introduction, I'm not a native english speaker)*

The standart library of V is very rich and powerful, I will try to demonstrate some of the features of `time` module from the vlib, using examples.

To begin, we should create a `time_example.v` file and import the time module :
```go
import time
```

Now open this [page](https://modules.vlang.io/time.html) to check the functions of the `time` module, we're gonna use some of them in this example file. you can add every example of these in a new line.

#### Create a time variable that contains actual time:
```go
t := time.now() // for me, t is 2024-03-31 21:01:45
```

#### You can format your date to `YYYY-MM-DD HH:mm`
```go
println(t.format()) // returns 2024-03-31 21:01
```

#### To custom format your date you can use `custom_format()` :
```go
println(t.custom_format("dddd DD/MMM/YYYY")) // returns Sunday 31/Mar/2024
```

#### To return only hours and minutes from `t` :
```go
println(t.hhmm()) // returns 21:01
```

#### To get Unix timestamp (seconds from 1970-01-01) use `unix_time()`:
```go
println(t.unix_time()) // returns 1711918894
```

#### To get only the date in European style use `ddmmy()` :
```go
println(t.ddmmy()) // returns 31.03.2024
```

#### To  get  the month and the day in US format use `md()`:
```go
println("hello user it's ${t.md()}") // returns hello user it's March 31
```

#### To check if the year is leap use `is_leap_year()`:
```go
println(t.is_leap_year()) // returns true
```


Let's create another variable with an earlier date, for that we can use parse() and provide a date string in this format "YYYY-MM-DD HH:mm:ss", a `!` or a `or {}` should append this function.
since we're gonna modify this variable later, we should declare it with `mut`

```go
mut h := time.parse("2023-10-07 05:10:00")!
```
#### To get the Weekday of a date:
```go
println(h.weekday_str()) // returns Sat
```

#### To get a long description of the difference between an earlier date and actual date:
```go
println(h.relative()) // returns last Oct 7
```

#### To get a short description of the difference:
```go
println(h.relative_short()) // returns 176d ago
```

#### To get the duration from `h`  in format `hours:minutes:seconds` :
```go
println(time.since(h)) // returns 4241:09:17
```

#### To get duration in minutes only as a f64:
```go
println(time.since(h).minutes()) // returns 254469.2992380094
```

#### You can calculate the duration between two dates by using `-` :
```go
println(t - h) // return 4241:09:17
```

#### You can compare two dates using `==` or `<` :
```go
println(t == h) // returns false
println(h < t) // returns true
```

#### Add duration in nanoseconds to a time variable:
```go
println(h.add(1234567890 * 1000000)) // returns 2023-10-21 12:06:07
```

#### Add days to a time variable :
```go
println(h.add_days(90)) // returns 2024-01-05 05:10:00
```


The `time` module still contains a lot of functions that could make your DX very fast and productive.  
You can continue playing with the **stopwatch** feature, you can check it on : https://modules.vlang.io/time.html

Don't forget, **Vlang** is still WIP, so probably  more useful functions will be added to this module.
