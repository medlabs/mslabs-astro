import type { IFrontmatter } from '@/components';
import { PostContent, PostHeader, Section } from '@/components';
import type { JSX } from 'solid-js';

import { AppConfig } from '@/utils/AppConfig';

type IBlogPostProps = {
  frontmatter: IFrontmatter;
  children: JSX.Element;
};

export default function BlogPost (props: IBlogPostProps) {
    return (
  <Section>
    <PostHeader content={props.frontmatter} author={AppConfig.author} />

    <PostContent content={props.frontmatter}>{props.children}</PostContent>
  </Section>
);
};
