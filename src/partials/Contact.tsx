import {Section, ContactForm} from '@/components'

const Contact = () => (
  <Section>
    <ContactForm />
  </Section>
)

export {Contact}
